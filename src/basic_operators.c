#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "grid.h"

#include "basic_operators.h"

void
calc_wsum_generic(double* output,
                  const struct GridSubset* subset,
                  const struct GridConnectivity* connectivity,
                  const double *values,
                  const double *weights,
                  const size_t level_count)
{
#pragma omp parallel for
  for (size_t output_idx = 0; output_idx < subset->count; ++output_idx)
  {
    const size_t entity_idx = subset->indices[output_idx];
    for (size_t level_idx = 0; level_idx < level_count; ++level_idx)
    {
      double result = 0.0;
      for (size_t connect_idx = 0; connect_idx < connectivity->tgt_count; ++connect_idx)
      {
        const size_t flat_connect_idx = entity_idx * connectivity->tgt_count + connect_idx;
        const size_t flat_values_idx = connectivity->matrix[flat_connect_idx] * level_count + level_idx;

        result += values[flat_values_idx] * weights[flat_connect_idx];
      }

      output[output_idx * level_count + level_idx] = result;
    }
  }
}

void
calc_wsum_triangle(double* output,
                   const struct GridSubset* subset,
                   const struct GridConnectivity* connectivity,
                   const double *values,
                   const double *weights,
                   const size_t level_count)
{
  assert(connectivity->tgt_count == 3);

#pragma omp parallel for
  for (size_t output_idx = 0; output_idx < subset->count; ++output_idx)
  {
    const size_t entity_idx = subset->indices[output_idx];
    const size_t first_connect_idx = entity_idx * connectivity->tgt_count;
    const size_t *values_idcs = &connectivity->matrix[first_connect_idx];

    for (size_t level_idx = 0; level_idx < level_count; ++level_idx)
    {
      output[output_idx * level_count + level_idx] =
        values[values_idcs[0] * level_count + level_idx] * weights[first_connect_idx + 0] +
        values[values_idcs[1] * level_count + level_idx] * weights[first_connect_idx + 1] +
        values[values_idcs[2] * level_count + level_idx] * weights[first_connect_idx + 2];
    }
  }
}

